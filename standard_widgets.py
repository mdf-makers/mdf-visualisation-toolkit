import panel as pn
import datetime as dt
site_name = pn.widgets.Select(
    name="Site", options=['tiksi','nyalesund',"eureka"],value = 'tiksi', margin=(0, 20, 0, 0)
)

#model_name = widgets[1]
model_name = pn.widgets.Select(
    name="model", options=['slav-rhmc','ifs-ecmwf'], margin=(0, 20, 0, 0)
)

#user selection variable
variable = pn.widgets.Select(
    name="Variables", options=['tas','ts','meep'], margin=(0, 20, 0, 0)
)

#user selection start and end date only sop 1 as of right now
dates = pn.widgets.DateRangeSlider(
    name='Date Range Slider',
    start=dt.datetime(2018, 2, 1), end=dt.datetime(2018, 3, 31),
    value=(dt.datetime(2018, 2, 1), dt.datetime(2018, 2, 2))
)

#user selection start time for model 
start_time = pn.widgets.Select(
    name="Start time", options=['00','12'], margin=(0, 20, 0, 0)
)

#user selection of which day should be fetched from the model files
#for concat
concat_day = pn.widgets.Select(
    name="Model day selected", options={1:0,2:1,3:2}, margin=(0,20,0,0)
)

loading = pn.indicators.LoadingSpinner(value=False, width=100, height=100)