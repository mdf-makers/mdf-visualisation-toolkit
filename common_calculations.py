import pandas as pd
import numpy as np



#function responsible for the merging of the dataframes containing the model and observation data. 
#This gets rid of any points between them where there is no direct timestep match.
def merge_data(var_obs,var_mod,variable):
    #rename column names to easier differentiate between values when combined
    var_obs = var_obs.rename(columns={variable:"var_obs"})
    var_mod = var_mod.rename(columns={variable:"var_mod"})
    
    #merges the dataframes based on matching time points, gets rid of all mismatches for time
    if len(var_obs.index) < len(var_mod):
        merged = var_obs.merge(var_mod, left_index=True, right_index=True, how='inner')
    else:
        merged = var_mod.merge(var_obs, left_index=True, right_index=True, how='inner')

    return merged


#The following cell contains the functions responsible for calcuating the windspeed, ustar, scaled heat-flux and temperature/humidity gradient. 
#These functions are used in the three sub-cases producing scatter plots for:
#* Ustar vs. windspeed
#* Scaled heat-flux vs. temperature gradient
#* Scaled watervapor-flux vs. humidity gradient
def calc_windspeed(u_a,v_a):
    #only for 10-m
    u_a.columns = ['comp']
    v_a.columns = ['comp']
    windspeed_calc = lambda u,v: (np.sqrt(u*u+v*v))
    windspeed = u_a.combine(v_a,windspeed_calc)
    return windspeed

def calc_ustar(tau_u,tau_v):
    #only for 10-m
    tau_u.columns = ['comp']
    tau_v.columns = ['comp']
    u_star_calc = lambda u,v: (np.sqrt(np.sqrt(u*u + v*v)))
    ustar = tau_u.combine(tau_v,u_star_calc)
    
    return ustar

def calc_scaledHeatFlux(hf,wspeed):
    hf.columns = ['comp']
    wspeed.columns = ['comp']
    scaledHF_calc = lambda u,v: (u/v)
    scaledHF = hf.combine(wspeed, scaledHF_calc)
    return scaledHF

def calc_gradient(ref, s):
    ref.columns = ['comp']
    s.columns = ['comp']
    calc_grad = lambda u,v: (u-v)
    grad = ref.combine(s,calc_grad)
    return grad


def calc_fc_err(var_mod,var_obs,variable):
    data = pd.DataFrame()
    var_mod = var_mod.rename(columns={variable:variable+"_mod"})
    var_obs = var_obs.rename(columns={variable:variable+"_obs"})
    
    if len(var_obs.index) < len(var_mod):
        merged = var_obs.merge(var_mod, left_index=True, right_index=True, how='inner')
    else:
        merged = var_mod.merge(var_obs, left_index=True, right_index=True, how='inner')
    data['err_' + variable] = merged[variable+'_mod'] - merged[variable+'_obs']
    
    return data